import os
from flask import Flask, jsonify, request

app = Flask(__name__)

@app.route('/')
def root():
    return "Hello World"

@app.route('/messenger/', methods=['GET'])
def messenger():
    text = {
        "messages": [
            {
                "text": request.args.get('text')
            }
        ]
    }
    return jsonify(text)

if __name__ == '__main__':
    app.port = int(os.environ.get('PORT', 5000))
    app.debug = True
    app.run()
